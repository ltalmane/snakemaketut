rule intersect_muts_with_blacklist:
    input:
        muts = "data/muts.{genome}.bed",
        regions = "data/{tf}_chip_{genome}.sorted.bed",
        blacklist = "data/{tf}_chip_{genome}.blacklist.bed"
    output:
        tfbsMuts = "data/{tf}_chip_{genome}.muts.bed"
    shell:
        "bedtools intersect -a {input.muts} -b {input.regions} | bedtools intersect -v -a stdin -b {input.blacklist} > {output.tfbsMuts}"


rule plot_hist:
  input:
    files = lambda wildcards : expand("data/{tf}_chip_{genome}.muts.bed", tf=TF_list , genome = wildcards.genome) # TF_list is defined in the Snakefile
  output:
    stats = "stats/mut_dist.{bin}.{genome}.pdf"
  params:
    files = lambda wildcards, input : ",".join(input.files)
  conda:
    "envR.yaml"
  shell:
    "Rscript scripts/countMuts.R -f {params.files} -o {output.stats} -b {wildcards.bins}"


rule reformat_file:
    input:
        nochr = "data/{tf}_chip_{genome}_noCHR.bed"
    output:
        chr = "data/{tf}_chip_{genome}.bed"
    run:
        with open(input.nochr) as file, open(output.chr,"w") as outfile:
            for line in file:
                outfile.write("chr" + line)

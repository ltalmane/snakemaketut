# Inroduction to Snakemake

----------------------

* During this course we will lean how to use Snakemake by **progressively building** a data analysis wokflow
* This GIT repository which already has out **‘starting point’** scripts and some data 
* As we go along, I will explain what contents of those files mean 
* I will ask you to perform some **ACTIONS** along the way 
* Some of those going to be **executing snakemake** on the command line 
* And some are going to be **editing scripts** to build up the workflow
 
------------------
* Sometimes I will ask you to add large chunks of code into your scripts
* Those you can just copy from the `add_code.txt` file 
* There is also a `help/` folder, which contains subfolders where script files look like they are meant to look like after you completed an ACTION
* So if you are struggling, you can copy contents of file in the `help/` directory into the files we are working with 
* I will indicate the names of those when specifying ACTIONS

-------------------

First you will need to copy this git repository 

`git clone https://git.ecdf.ed.ac.uk/ltalmane/snakemaketut.git`


------------------

*During this course we will progressively build a workflow that does following things:*


1.  Sort files with ChIP-seq peaks for several transcription factors (TFs)

2.  Determine where in the those peaks mutations are located (intersect peaks with mutations)

3.  Plot the distribution of mutations across all those ChIP-seq peaks for all TFs 

4. Include option to reformat ChIP-seq file if we don’t have one in right format

5. Include option to filter peaks through blacklist for TFs where there is one

6. Do all of this for hg38 and hg19 genome assemblies 



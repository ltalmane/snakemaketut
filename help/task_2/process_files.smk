wildcard_constraints:
  genome = "(hg19|hg38)"

rule sort_chip:
  input:
    "data/{tf}_chip_{genome}.bed"
  output:
    temp("data/{tf}_chip_{genome}.sorted.bed")
  shell:
    "sort {input} > {output}"


rule intersect_muts:
    input:
        muts = "data/muts.{genome}.bed",
        regions = "data/{tf}_chip_{genome}.sorted.bed"
    output:
        tfbsMuts = "data/{tf}_chip_{genome}.muts.bed"
    shell:
        "bedtools intersect -wo -a {input.muts} -b {input.regions} > {output.tfbsMuts}"

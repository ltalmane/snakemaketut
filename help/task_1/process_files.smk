rule sort_chip:
  input:
    "data/CTCF_chip_hg38.bed"
  output:
    "data/CTCF_chip_hg38.sorted.bed"
  shell:
    "sort {input} > {output}"


rule intersect_muts:
    input:
        muts = "data/muts.hg38.bed",
        chips = "data/CTCF_chip_hg38.sorted.bed"
    output:
        tfbsMuts = "data/CTCF_chip_hg38.muts.bed"
    shell:
        "bedtools intersect -wo -a {input.muts} -b {input.chips} > {output.tfbsMuts}"
